// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
import Vue from 'vue'
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
import App from './App'
import router from './router'
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
//import Header from './components/Header.vue'


/*******************************************************
 * Vue2Leaflet
 * *****************************************************/
import 'iview/dist/styles/iview.css'    // 使用 CSS
import Vue2Leaflet from 'vue2-leaflet'
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.component('v-map', Vue2Leaflet.Map)
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.component('v-tilelayer', Vue2Leaflet.TileLayer)
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.component('v-marker', Vue2Leaflet.Marker)
Vue.component('Header', require ('./components/Header.vue'))
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.use(Vue2Leaflet)


/*******************************************************
 * iView
 * *****************************************************/
import iView from 'iview'
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.use(iView)


/*******************************************************
 * vuex
 * *****************************************************/
import Vuex from 'vuex'
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.use(Vuex)
//noinspection JSUnresolvedFunction,JSUnresolvedVariable
const store = new Vuex.Store({
  state: {
    count: 0,
    book: 0,
    userInfo: null,
    isLogin: false
  },
  mutations: {
    increment (state) {
      state.count++
    },
    update (state, obj){
      Object.assign(state, obj)
    }
  },
  action:{}
})


//noinspection JSUnresolvedFunction,JSUnresolvedVariable
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
